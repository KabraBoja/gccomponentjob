//
//  TestJobTimer.m
//  GCJobs
//
//  Created by Eloi Guzmán on 22/10/13.
//  Copyright (c) 2013 Eloi Guzmán. All rights reserved.
//

#import "TestJobTimer.h"
#import "GCError.h"

@implementation TestJobTimer

-(void)jobStarts
{
    //float random0a1 = rand() / (float)RAND_MAX;
    _timer = [NSTimer timerWithTimeInterval:2.0 target:self selector:@selector(fire:) userInfo:nil repeats:NO];
    NSRunLoop *runLoop = [NSRunLoop currentRunLoop];
    [runLoop addTimer:_timer forMode:NSDefaultRunLoopMode];

    /*_timer = [NSTimer timerWithTimeInterval:random0a1 target:self selector:@selector(fire2:) userInfo:nil repeats:NO];
    [runLoop addTimer:_timer forMode:NSDefaultRunLoopMode];*/
    [runLoop run];
}

-(void)fire:(id)sender
{
    NSLog(@"Fire!");
    //[self addError:[NSError errorWithDomain:GCComponentJobErrorDomain code:GCComponentJobErrorCodeFailed userInfo:nil]];
    [self addComponentError:[GCError errorWithError:[NSError errorWithDomain:GCComponentJobErrorDomain code:GCComponentJobErrorCodeFailed userInfo:nil] sender:self]];
    [self finishJob];
}

-(void)fire2:(id)sender
{
    [self cancelJob];
}


-(void)dealloc
{
    
}

@end
