//
//  TestJobTimer.h
//  GCJobs
//
//  Created by Eloi Guzmán on 22/10/13.
//  Copyright (c) 2013 Eloi Guzmán. All rights reserved.
//

#import "GCJob.h"

@interface TestJobTimer : GCJob
{
    NSTimer * _timer;
}
@end
