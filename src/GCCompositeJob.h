//
//  GCCompositeJob.h
//  mrbricolage
//
//  Created by Eloi Guzmán on 11/10/13.
//  Copyright (c) 2013 Luis Lopez. All rights reserved.
//

#import "GCComponentJob.h"
#import "GCSenderCollection.h"


/*!
 
 This composite made of some GCComponentJobs.
 
 */
@interface GCCompositeJob : GCComponentJob<GCJobStateProtocol,GCJobCompletionStatusProtocol>
{
    NSOperationQueue * _queue;
    //SenderCollection * _jobsCollection;
    NSMutableArray * _jobsFinished;
    NSMutableArray * _jobsToDo;
    GCSenderCollection * _allJobs;
}

@property(nonatomic,weak) id<GCJobCompletionStatusProtocol> delegateCompletion;

-(NSArray*)getFinishedJobs;
-(GCComponentJob*)getLastFinished;

-(BOOL)addJob:(GCComponentJob*)job;
-(BOOL)addJobs:(NSArray*)jobs;
-(void)start;
-(NSArray*)failedJobs;


@end
