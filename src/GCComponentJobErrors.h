//
//  GCComponentJobErrors.h
//  GCJobs
//
//  Created by Eloi Guzmán on 25/10/13.
//  Copyright (c) 2013 Eloi Guzmán. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const GCComponentJobErrorDomain;

typedef enum GCComponentJobErrorCodes
{
    GCComponentJobErrorCodeFailed = 0, /* General error in a job */
    
}GCComponentJobErrorCodes;
