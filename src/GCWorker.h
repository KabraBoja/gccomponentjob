//
//  GCWorker.h
//  mrbricolage
//
//  Created by Eloi Guzmán on 11/10/13.
//  Copyright (c) 2013 Luis Lopez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GCCompositeJob.h"
#import "GCSenderCollection.h"

@class GCWorker;

typedef enum GCWorkerStatus
{
    GCWorkerStatusFree,
    GCWorkerStatusWorking,
}GCWorkerStatus;

/**
 Protocol that delegates the job states transitions by the worker
 */
@protocol GCWorkerJobStateProtocol <NSObject>

-(void)gc_Worker:(GCWorker*)worker jobReady:(GCComponentJob*)job;
-(void)gc_Worker:(GCWorker*)worker jobExecuting:(GCComponentJob*)job;
-(void)gc_Worker:(GCWorker*)worker jobFinished:(GCComponentJob*)job;

@end


@interface GCWorker : NSObject<GCJobCompletionStatusProtocol,GCJobStateProtocol>
{
    GCSenderCollection * _collectionActiveJobs;
    //SenderCollection * _collectionOfStateDelegatesByJob;
    //SenderCollection * _collectionOfCompletionDelegatesByJob;
    NSOperationQueue * _queue;
}

@property(atomic,assign) GCWorkerStatus status;
@property(nonatomic,weak) id<GCWorkerJobStateProtocol> delegateState;

-(void)addJob:(GCComponentJob*)job;

@end
