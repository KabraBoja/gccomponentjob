//
//  GCCompositeJob.m
//  mrbricolage
//
//  Created by Eloi Guzmán on 11/10/13.
//  Copyright (c) 2013 Luis Lopez. All rights reserved.
//

#import "GCCompositeJob.h"
#import "GCComponentJob_Protected.h"

@implementation GCCompositeJob

-(id)initWithParent:(id<GCJobStateProtocol,GCJobCompletionStatusProtocol>)parent identifier:(NSString *)identifier
{
    self = [super initWithParent:parent identifier:identifier];
    if (self) {
        _state = GCJobStateReady;
        _queue = [[NSOperationQueue alloc]init];
        [_queue setSuspended:YES];
        _jobsFinished = [[NSMutableArray alloc]init];
        _jobsToDo = [[NSMutableArray alloc]init];
        _allJobs = [[GCSenderCollection alloc]initWithWeakReferences];
    }
    return self;
}


-(BOOL)addJob:(GCComponentJob*)job
{
    [_lock lock];
    BOOL success = NO;
    NSAssert(job.parent == self,@"The job parent has to be this Composite job");
    if (job.parent == self) {
        if (_state == GCJobStateReady)
        {
            [_jobsToDo addObject:job];
            [_allJobs addSender:job forKeyObject:job];
            success = YES;
        }
    }
    [_lock unlock];
    return success;
    
}

-(BOOL)addJobs:(NSArray*)jobs
{
    [_lock lock];
    BOOL success = YES;
    if (_state == GCJobStateReady) {
        for (GCComponentJob * job in jobs) {
            NSAssert([job isKindOfClass:GCComponentJob.class],@"No GCComponentJob class.");
            success = success && [self addJob:job];
        }
    }else{
        success = NO;
    }
    
    [_lock unlock];
    return success;
}

-(void)start
{
    [_lock lock];
    [self notifyState];
    if (_state == GCJobStateReady) {
        [self setExecuting];
        [self notifyState];
        
        if (_jobsToDo.count > 0) {
            GCComponentJob * job = _jobsToDo[0];
            [_queue addOperation:job];
            [_queue setSuspended:NO];
        }else{
            [self setFinished];
            [self notifyState];
        }
    }
    [_lock unlock];
}


-(void)dealloc
{
    [_jobsFinished removeAllObjects];
    [_jobsToDo removeAllObjects];
    //    T21LogDealloc(@"");
}

-(void)gc_JobFinished:(GCComponentJob *)job
{
    [_lock lock];
    NSAssert(job,@"Job cannot be nil");
    NSAssert(_jobsToDo.count > 0,@"If a job has finnished, there must be at least one job to do (the finnishing one)");
    NSAssert([_jobsToDo containsObject:job],@"The job finnished is not performed/owned by this CompositeJob");
    
    [_jobsFinished addObject:job];
    [_jobsToDo removeObjectAtIndex:0];
    
    //if the last job failed with error
    GCComponentError * jobFinishedError = [job getComponentError];
    if (jobFinishedError) {
        if (!_errorComposite) {
            _errorComposite = [GCCompositeError errorWithComponentError:jobFinishedError sender:self];
        }else{
            [_errorComposite addComponentError:jobFinishedError];
        }
    }
    
    if ([_jobsToDo count] == 0) {
        //Notify
        [super setFinished];
        [self notifyState];
    }else{
        GCComponentJob * nextJob = _jobsToDo[0];
        nextJob.previousJob = job;
        NSAssert([nextJob isKindOfClass:[GCComponentJob class]],@"This object is not a GCComponentJob");
        [_queue addOperation:nextJob];
    }
    [_lock unlock];
}

-(void)gc_JobExecuting:(GCComponentJob *)job
{
    
}

-(void)gc_JobReady:(GCComponentJob *)job
{
    
}


-(NSArray*)getFinishedJobs
{
    [_lock lock];
    NSArray * array = [NSArray arrayWithArray:_jobsFinished];
    [_lock unlock];
    return array;
}

-(GCComponentJob *)getLastFinished
{
    [_lock lock];
    GCComponentJob * j = nil;
    NSArray * finishedJobs = [self getFinishedJobs];
    if (finishedJobs.count > 0) {
        j = finishedJobs[0];
    }
    [_lock unlock];
    return j;
}

-(void)gc_Job:(GCComponentJob *)job updatedCompletionPercentage:(CGFloat)percent withInfo:(id)completionInfo
{
    [_lock lock];
    float jobPartPerc = 100.0f / MAX(_allJobs.count,1);
    float perc = jobPartPerc * _jobsFinished.count;
    perc += jobPartPerc * (MAX(0.0f,MIN(100.0f, percent)) / 100.0);
    [_lock unlock];
    
    //T21LogDebug(@"Composite job completed %d %%",(int)perc);
    [super setCompletionPercentage:perc withInfo:completionInfo];    
}

-(NSArray*)failedJobs
{
    [_lock lock];
    NSMutableArray * failedJobs = [[NSMutableArray alloc]init];
    for (GCComponentJob * job in _jobsFinished) {
        if ([job getComponentError]) {
            [failedJobs addObject:job];
        }
    }
    [_lock unlock];
    return failedJobs;
}

-(GCComponentError *)getComponentError
{
    return _errorComposite;
}

/*
 -(NSError *)error
{
    [_lock lock];
    NSError * error = nil;
    if ([self failedJobs].count > 0) {
        GCComponentJob * failedJob = [self.failedJobs objectAtIndex:0];
        error = [[failedJob getComponentError]error];
    }
    [_lock unlock];
    return error;
}

-(NSArray *)errors
{
    [_lock lock];
    NSMutableArray * errors = [[NSMutableArray alloc]init];
    for (GCComponentJob * job in _jobsFinished) {
        if ([job getComponentError]) {
            [errors addObject:[[job getComponentError]error]];
        }
    }
    return errors;
    [_lock unlock];
}
 */

-(NSString *)description
{
    NSMutableString * desc = [NSMutableString stringWithFormat:@"%@ ",[super description]];
    [desc appendFormat:@"childsCount: %d \n",_allJobs.count];
    return desc;
}

@end
