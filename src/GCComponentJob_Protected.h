//
//  GCComponentJob_Protected.h
//  GCJobs
//
//  Created by Eloi Guzmán on 22/10/13.
//  Copyright (c) 2013 Eloi Guzmán. All rights reserved.
//

#import "GCComponentJob.h"


/**
 The following methods can only be called by subclasses of GCComponentJob class. Never get called by clients.
 */

@interface GCComponentJob ()

-(void)setFinished;
-(void)setExecuting;
-(void)setCancelled;
-(void)notifyState;
-(void)setPreviousJob:(GCComponentJob*)job;

@end
