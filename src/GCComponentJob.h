//
//  GCComponentJob.h
//  mrbricolage
//
//  Created by Eloi Guzmán on 10/10/13.
//  Copyright (c) 2013 Luis Lopez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GCCompositeError.h"
#import "GCComponentJobErrors.h"

typedef enum GCJobState{
    GCJobStateReady       = 0,
    GCJobStateExecuting   = 1,
    GCJobStateFinished    = 2,
}GCJobState;


@class GCComponentJob;


/*!
 
 Protocol that delegates the completion percentage by the job
 
 */
@protocol GCJobCompletionStatusProtocol <NSObject>

/*!
 
 This is a function.
 
 */
-(void)gc_Job:(GCComponentJob*)job updatedCompletionPercentage:(CGFloat)percent withInfo:(id)completionInfo;

@end


/*!
 
 Protocol that delegates the state transitions by the job
 
 */
@protocol GCJobStateProtocol <NSObject>

@optional
-(void)gc_JobReady:(GCComponentJob*)job;
-(void)gc_JobExecuting:(GCComponentJob*)job;
-(void)gc_JobFinished:(GCComponentJob*)job;

@end


/**
 
 This class isherits from a NSOperation. To use it, you must override the method jobStarts. Inside this method you
 will implement all the synchronous/asynchronous task. To ensure that the job (NSOperation) will end properly you must
 call the method finishJob at all possible end points of the task.
 
 */
@interface GCComponentJob : NSOperation
{
    GCJobState _state;
    GCCompositeError * _errorComposite;
    NSRecursiveLock * _lock;
    NSString * _identifier;
}

@property(nonatomic,assign,readonly) GCJobState state;
@property(atomic,assign,readonly) BOOL jobCancelled;
@property(nonatomic,weak) GCComponentJob * previousJob;
@property(nonatomic,assign,readonly) CGFloat completionPercentage;

@property(nonatomic,weak) id<GCJobStateProtocol> delegateState;
@property(nonatomic,weak) id<GCJobCompletionStatusProtocol> delegateCompletion;
@property(nonatomic,weak,readonly) id<GCJobStateProtocol,GCJobCompletionStatusProtocol> parent;

#pragma mark - Init methods
+(id)jobWithParent:(id<GCJobStateProtocol,GCJobCompletionStatusProtocol>)parent identifier:(NSString*)identifier;
+(id)jobWithParent:(id<GCJobStateProtocol,GCJobCompletionStatusProtocol>)parent;
-(id)initWithParent:(id<GCJobStateProtocol,GCJobCompletionStatusProtocol>)parent identifier:(NSString*)identifier;
-(id)initWithParent:(id<GCJobStateProtocol,GCJobCompletionStatusProtocol>)parent;


+(dispatch_queue_t)dispatchQueueJobSynchronization;

#pragma mark - LifeCycle management
-(NSString*)getIdentifier;

#pragma mark - Error management
-(GCComponentError*)getComponentError;

#pragma mark - Job managemenet
-(void)jobStarts;
-(void)finishJob;
-(void)cancelJob;
-(void)setCompletionPercentage:(CGFloat)completionPercentage withInfo:(id)info;

@end

