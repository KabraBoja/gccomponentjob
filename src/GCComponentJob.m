//
//  GCComponentJob.m
//  mrbricolage
//
//  Created by Eloi Guzmán on 10/10/13.
//  Copyright (c) 2013 Luis Lopez. All rights reserved.
//

#import "GCComponentJob.h"
#import "GCComponentJob_Protected.h"
#import "GCError.h"

@implementation GCComponentJob

@synthesize previousJob = _previousJob;

+(dispatch_queue_t)dispatchQueueJobSynchronization
{
    static dispatch_queue_t _dispatchQueueJobSynchronization;
    static dispatch_once_t _oncePredicate;
    dispatch_once(&_oncePredicate, ^{
        _dispatchQueueJobSynchronization = dispatch_queue_create("GCComponentJobSync", NULL);
    });
    return _dispatchQueueJobSynchronization;
}

+(id)jobWithParent:(id<GCJobStateProtocol,GCJobCompletionStatusProtocol>)parent
{
    GCComponentJob * job = [[self.class alloc]initWithParent:parent identifier:nil];
    return job;
}

+(id)jobWithParent:(id<GCJobStateProtocol,GCJobCompletionStatusProtocol>)parent identifier:(NSString*)identifier
{
    GCComponentJob * job = [[self.class alloc]initWithParent:parent identifier:identifier];
    return job;
}


-(id)initWithParent:(id<GCJobStateProtocol,GCJobCompletionStatusProtocol>)parent
{
    self = [self initWithParent:parent identifier:nil];
    if (self) {
    }
    return self;
}


-(id)initWithParent:(id<GCJobStateProtocol,GCJobCompletionStatusProtocol>)parent identifier:(NSString*)identifier
{
    self = [super init];
    if (self) {
        _lock = [[NSRecursiveLock alloc]init];
        _parent = parent;
        _state = GCJobStateReady;
        _completionPercentage = 0.0f;
        _jobCancelled = NO;
        if (!identifier) {
            _identifier = [NSString stringWithFormat:@"%p",self];
        }else{
            _identifier = identifier;
        }
    }
    return self;
}

-(NSString*)getIdentifier
{
    return _identifier;
}

-(id)init
{
    NSAssert(false,@"You must use initWithParent in order to define a relationship");
    self = [super init];
    if (self) {

    }
    return self;
}

-(void)dealloc
{
    [self cancelNotify:NO];
    //T21LogDealloc(@"Job %@ deallocated",NSStringFromClass(self.class));
    NSLog(@"Job %@ deallocated",NSStringFromClass(self.class));
}

-(void)setFinished
{
    [_lock lock];
    NSAssert(_state == GCJobStateExecuting || _state == GCJobStateReady || _state == GCJobStateFinished,@"State transition not permitted");
    NSLog(@"Finishing Job %@",NSStringFromClass(self.class));
    [self willChangeValueForKey:@"isFinished"];
    _state = GCJobStateFinished;
    [self didChangeValueForKey:@"isFinished"];
    [_lock unlock];
}

-(void)setExecuting
{
    NSAssert(_state == GCJobStateReady,@"State transition not permitted");
    [_lock lock];
    NSLog(@"Starting Job %@",NSStringFromClass(self.class));
    [self willChangeValueForKey:@"isExecuting"];
    _state = GCJobStateExecuting;
    [self didChangeValueForKey:@"isExecuting"];
    [_lock unlock];
}

-(void)setCancelled
{
    NSAssert(_state == GCJobStateReady || _state == GCJobStateExecuting ,@"State transition not permitted");
    [_lock lock];
    NSLog(@"Cancelling Job %@",NSStringFromClass(self.class));
    [self willChangeValueForKey:@"isCancelled"];
    _jobCancelled = YES;
    _state = GCJobStateFinished;
    [super cancel];
    [self didChangeValueForKey:@"isCancelled"];
    [_lock unlock];
}

-(void)setPreviousJob:(GCComponentJob *)previousJob
{
    [_lock lock];
    NSAssert(self.state == GCJobStateReady,@"The job must be ready to modify the previous job");
    if (self.state == GCJobStateReady) {
        _previousJob = previousJob;
    }
    [_lock unlock];
}

#pragma mark - Delegates

-(void)setDelegateState:(id<GCJobStateProtocol>)delegateState
{
    NSAssert(delegateState != self.parent,@"You cannot set the parent as a delegate, already controlled interanlly");
    _delegateState = delegateState;
}

#pragma mark - Error management

-(GCComponentError *)getComponentError
{
    return _errorComposite;
}


#pragma mark - Subclassing methods

-(BOOL)isReady {
    return _state == GCJobStateReady && [super isReady];
}

-(BOOL)isExecuting {
    return _state == GCJobStateExecuting;
}

-(BOOL)isFinished {
    return _state == GCJobStateFinished;
}

-(BOOL)isConcurrent {
    return YES;
}

-(void)cancel{
    [self cancelNotify:YES];
}



-(void)cancelNotify:(BOOL)notify{
    [_lock lock];
    if (![self isCancelled] && ![self isFinished]) {
        [self setCancelled];
        if (notify) {
            [self notifyState];
        }
    }
    [_lock unlock];
}

-(void)cancelJob
{
    [self cancel];
}

-(void)start
{
    //_startedInQueue = [NSOperationQueue currentQueue];
    [_lock lock];
    if ([self isReady]) {
        
        //We set the operation to running
        [self setExecuting];
        
        //notifyState starting
        [self notifyState];
        
        //We start the action
        [self jobStarts];
        
    }else if([self isCancelled])
    {
        [self notifyState];
    }
    [_lock unlock];
}


-(void)notifyState
{
    [_lock lock];
    switch (self.state) {
        case GCJobStateReady:
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([self.delegateState respondsToSelector:@selector(gc_JobReady:)]) {
                    [self.delegateState gc_JobReady:self];
                }
            });

            dispatch_async([GCComponentJob dispatchQueueJobSynchronization], ^{
                if ([self.parent respondsToSelector:@selector(gc_JobReady:)]) {
                    [self.parent gc_JobReady:self];
                }
            });
        }
            break;
            
        case GCJobStateExecuting:
        {
            dispatch_async(dispatch_get_main_queue(), ^{ 
                if ([self.delegateState respondsToSelector:@selector(gc_JobExecuting:)]) {
                    [self.delegateState gc_JobExecuting:self];
                }
            });
            
            dispatch_async([GCComponentJob dispatchQueueJobSynchronization], ^{
                
                if ([self.parent respondsToSelector:@selector(gc_JobExecuting:)]) {
                    [self.parent gc_JobExecuting:self];
                }
            });
            
        }
            break;
            
        case GCJobStateFinished:
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([self.delegateState respondsToSelector:@selector(gc_JobFinished:)]) {
                    [self.delegateState gc_JobFinished:self];
                }
            });
            
            dispatch_async([GCComponentJob dispatchQueueJobSynchronization], ^{
                if ([self.parent respondsToSelector:@selector(gc_JobFinished:)]) {
                    [self.parent gc_JobFinished:self];
                }
            });
        }
            break;
        default:
            break;
    }
    [_lock unlock];
}

//-(void)setDelegateState:(id<GCJobStateProtocol>)delegateState
//{
//    NSAssert([self isReady],@"A Job must be in ready state to change the property delegateState");
//    _delegateState = delegateState;
//}




-(void)finishJob
{
    [_lock lock];
    BOOL isAlreadyFinnished = NO;
    if (_state == GCJobStateFinished) {
        isAlreadyFinnished = YES;
    }
    
    if (!isAlreadyFinnished) {
        [self setFinished];
        [self notifyState];
    }
    [_lock unlock];
}

-(void)jobStarts
{
    //Override
    NSAssert(false,@"Overried this method");
}


#pragma mark - notifyState completion percentage delegate

-(void)setCompletionPercentage:(CGFloat)completionPercentage withInfo:(id)info
{
    [_lock lock];
    _completionPercentage = MAX(0.0f,MIN(100.0f, completionPercentage));
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([self.delegateCompletion respondsToSelector:@selector(gc_baseJob:updatedCompletionPercentage:withInfo:)]) {
            [self.delegateCompletion gc_Job:self updatedCompletionPercentage:_completionPercentage withInfo:nil];
        }
    });
    
    dispatch_async([GCComponentJob dispatchQueueJobSynchronization], ^{
        if ([self.parent respondsToSelector:@selector(gc_baseJob:updatedCompletionPercentage:withInfo:)]) {
            [self.parent gc_Job:self updatedCompletionPercentage:_completionPercentage withInfo:nil];
        }
    });
    [_lock unlock];
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"%@ id: %@",[super description],_identifier];
}


@end
