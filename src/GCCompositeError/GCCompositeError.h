//
//  GCCompositeError.h
//  GCJobs
//
//  Created by Eloi Guzmán on 22/10/13.
//  Copyright (c) 2013 Eloi Guzmán. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GCComponentError.h"
#import "GCSenderCollection.h"
#import "GCError.h"


@interface GCCompositeError : GCComponentError
{
    NSMutableOrderedSet * _errors;
}

#pragma mark - Init methods

-(id)initWithComponentError:(GCComponentError*)error sender:(id)sender;
-(id)initWithComponentError:(GCComponentError*)error;
+(id)errorWithComponentError:(GCComponentError*)error sender:(id)sender;
+(id)errorWithComponentError:(GCComponentError*)error;

-(id)initWithError:(NSError*)error sender:(id)sender;
-(id)initWithError:(NSError*)error;
+(id)errorWithError:(NSError*)error sender:(id)sender;
+(id)errorWithError:(NSError*)error;


#pragma mark - Error management
-(NSOrderedSet*)getErrors;
-(NSOrderedSet*)getComponentErrors;
-(void)addError:(NSError*)error;
-(void)addComponentError:(GCComponentError*)error;


@end
