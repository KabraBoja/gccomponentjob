//
//  GCComponentError.m
//  GCJobs
//
//  Created by Eloi Guzmán on 22/10/13.
//  Copyright (c) 2013 Eloi Guzmán. All rights reserved.
//

#import "GCComponentError.h"
#import "GCJob.h"
#import "GCCompositeJob.h"

@implementation GCComponentError

@synthesize sender = _sender;
@synthesize error = _error;

-(id)init
{
    NSAssert(false,@"Use constructor with error param");
    self = [super init];
    if (self) {
        
    }
    return self;
}

-(void)privateInit
{
    _lock = [[NSRecursiveLock alloc]init];
}

-(id)initWithError:(NSError*)error sender:(id)sender
{
    NSAssert(error,@"Error cannot be nil");
    self = [super init];
    if (self) {
        [self privateInit];
        _error = error;
        _sender = sender;
    }
    return self;
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"%@ error: %@ sender: %@",[super description],[_error description],[_sender description]];
}

-(NSUInteger)containsErrorWithDomain:(NSString*)domain code:(NSInteger)code checkCode:(BOOL)checkCode recurisve:(BOOL)recursive errors:(NSMutableArray**)errors
{
    NSAssert(false,@"This can only be called by subclasses");
    return 0;
}

-(NSUInteger)containsErrorWithDomain:(NSString*)domain code:(NSInteger)code recurisve:(BOOL)recursive errors:(NSMutableArray**)errors
{
    if (!errors) {
        *errors = [NSMutableArray array];
    }
    return [self containsErrorWithDomain:domain code:code checkCode:YES recurisve:recursive errors:errors];
}

-(NSUInteger)containsErrorWithDomain:(NSString*)domain recurisve:(BOOL)recursive errors:(NSMutableArray**)errors
{
    return [self containsErrorWithDomain:domain code:0 checkCode:NO recurisve:recursive errors:errors];
}

-(NSUInteger)containsErrorWithDomain:(NSString*)domain code:(NSInteger)code recurisve:(BOOL)recursive
{
    return [self containsErrorWithDomain:domain code:code checkCode:YES recurisve:recursive errors:nil];
}

-(NSUInteger)containsErrorWithDomain:(NSString*)domain code:(NSInteger)code
{
    return [self containsErrorWithDomain:domain code:code checkCode:YES recurisve:NO errors:nil];
}

-(NSUInteger)containsErrorWithDomain:(NSString*)domain recurisve:(BOOL)recursive
{
    return [self containsErrorWithDomain:domain code:0 checkCode:NO recurisve:recursive errors:nil];
}

-(NSUInteger)containsErrorWithDomain:(NSString*)domain
{
    return [self containsErrorWithDomain:domain code:0 checkCode:NO recurisve:NO errors:nil];
}





@end
