//
//  GCError.h
//  GCJobs
//
//  Created by Eloi Guzmán on 22/10/13.
//  Copyright (c) 2013 Eloi Guzmán. All rights reserved.
//

#import "GCComponentError.h"

@interface GCError : GCComponentError<NSCopying>

-(id)initWithError:(NSError*)error sender:(id)sender;
-(id)initWithError:(NSError*)error;
+(id)errorWithError:(NSError*)error sender:(id)sender;
+(id)errorWithError:(NSError*)error;

@end
