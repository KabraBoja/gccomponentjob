//
//  GCComponentError.h
//  GCJobs
//
//  Created by Eloi Guzmán on 22/10/13.
//  Copyright (c) 2013 Eloi Guzmán. All rights reserved.
//

#import "GCComponentErrors.h"

@interface GCComponentError : NSObject
{
    NSRecursiveLock * _lock;
    NSError * _error;
    __weak id _sender;
}

#pragma mark - Checking for errors
-(NSUInteger)containsErrorWithDomain:(NSString*)domain code:(NSInteger)code recurisve:(BOOL)recursive errors:(NSMutableArray**)errors;
-(NSUInteger)containsErrorWithDomain:(NSString*)domain recurisve:(BOOL)recursive errors:(NSMutableArray**)errors;
-(NSUInteger)containsErrorWithDomain:(NSString*)domain code:(NSInteger)code recurisve:(BOOL)recursive;
-(NSUInteger)containsErrorWithDomain:(NSString*)domain code:(NSInteger)code;
-(NSUInteger)containsErrorWithDomain:(NSString*)domain recurisve:(BOOL)recursive;
-(NSUInteger)containsErrorWithDomain:(NSString*)domain;

@property (nonatomic,strong,readonly) NSError * error;
@property (nonatomic,weak,readonly) NSError * sender;

@end
