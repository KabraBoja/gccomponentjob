//
//  GCError.m
//  GCJobs
//
//  Created by Eloi Guzmán on 22/10/13.
//  Copyright (c) 2013 Eloi Guzmán. All rights reserved.
//

#import "GCError.h"
#import "GCComponentError_Protected.h"

@implementation GCError

-(id)initWithError:(NSError*)error sender:(id)sender
{
    NSAssert(error,@"Error cannot be nil");
    self = [super initWithError:error sender:sender];
    if (self) {
        _error = error;
        _sender = sender;
    }
    return self;
}

-(id)initWithError:(NSError*)error
{
    self = [self initWithError:error sender:nil];
    return self;
}

+(id)errorWithError:(NSError*)error sender:(id)sender
{
    GCComponentError * e = [[GCError alloc] initWithError:error sender:sender];
    return e;
}

+(id)errorWithError:(NSError*)error
{
    GCComponentError * e = [[GCError alloc] initWithError:error sender:nil];
    return e;
}

-(id)copyWithZone:(NSZone *)zone
{
    return [GCError errorWithError:_error sender:_sender];
}

/*
-(NSUInteger)containsErrorWithDomain:(NSString*)domain code:(NSInteger)code checkCode:(BOOL)checkCode recurisve:(BOOL)recursive errors:(NSMutableArray**)errors
{
    if (!errors) {
        NSMutableArray * a = [NSMutableArray array];
        *errors = a;
    }

    BOOL domainFound = NO;
    BOOL codeFound = !checkCode;
    if ([domain isEqualToString:_error.domain]) {
        
    }
    
    if (checkCode) {
        if (code == _error.code) {
            codeFound = YES;
        }
    }
    return ;
}*/



@end
