//
//  GCComponentError_Protected.h
//  GCJobs
//
//  Created by Eloi Guzmán on 24/10/13.
//  Copyright (c) 2013 Eloi Guzmán. All rights reserved.
//

#import "GCComponentError.h"

@interface GCComponentError ()

-(void)privateInit;
-(id)initWithError:(NSError*)error sender:(id)sender;

@end
