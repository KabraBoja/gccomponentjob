//
//  GCComponentErrors.h
//  GCJobs
//
//  Created by Eloi Guzmán on 25/10/13.
//  Copyright (c) 2013 Eloi Guzmán. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const GCComponentErrorDomain;

typedef enum GCComponentErrorCodes
{
    GCComponentErrorCodeComposite = 0, /* Error contained by a composite error */
    
}GCComponentErrorCodes;