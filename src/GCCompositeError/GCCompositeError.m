//
//  GCCompositeError.m
//  GCJobs
//
//  Created by Eloi Guzmán on 22/10/13.
//  Copyright (c) 2013 Eloi Guzmán. All rights reserved.
//

#import "GCCompositeError.h"
#import "GCComponentError_Protected.h"

@implementation GCCompositeError

+(NSError*)getCompositeDefaultError
{
    return [NSError errorWithDomain:GCComponentErrorDomain code:GCComponentErrorCodeComposite userInfo:nil];
}

-(void)privateInit
{
    [super privateInit];
    _errors = [[NSMutableOrderedSet alloc]init];
}


-(id)initWithComponentError:(GCComponentError*)error sender:(id)sender
{
    NSAssert(error,@"Error cannot be nil");
    self = [super initWithError:[GCCompositeError getCompositeDefaultError] sender:sender];
    if (self) {
        [_errors addObject:error];
    }
    return self;
}

-(id)initWithComponentError:(GCComponentError*)error
{
    self = [self initWithComponentError:error sender:nil];
    return self;
}

+(id)errorWithComponentError:(GCComponentError*)error sender:(id)sender
{
    GCComponentError * newObj = [[GCCompositeError alloc]initWithComponentError:error sender:sender];
    return newObj;
}

+(id)errorWithComponentError:(GCComponentError*)error
{
    GCComponentError * newObj = [[GCCompositeError alloc]initWithComponentError:error sender:nil];
    return newObj;
}

-(id)initWithError:(NSError*)error sender:(id)sender
{
    NSAssert(error,@"Error cannot be nil");
    self = [super initWithError:[GCCompositeError getCompositeDefaultError] sender:sender];
    if (self) {
        [_errors addObject:[GCError errorWithError:error]];
    }
    return self;
}

-(id)initWithError:(NSError*)error
{
    self = [self initWithError:error sender:nil];
    return self;
}

+(id)errorWithError:(NSError*)error sender:(id)sender
{
    GCComponentError * e = [[GCCompositeError alloc] initWithError:error sender:sender];
    return e;
}

+(id)errorWithError:(NSError*)error
{
    GCComponentError * e = [[GCCompositeError alloc] initWithError:error sender:nil];
    return e;
}

-(id)copyWithZone:(NSZone *)zone
{
    GCCompositeError * copyObj = [GCCompositeError errorWithComponentError:_errors[0] sender:_sender];
    if (_errors.count > 1) {
        for (NSUInteger idx = 1; idx < _errors.count;idx++) {
            [copyObj addComponentError:_errors[idx]];
        }
    }
    return copyObj;
}



-(NSOrderedSet*)getComponentErrors
{
    [_lock lock];
    NSOrderedSet * componentErrors = [[NSOrderedSet alloc] initWithOrderedSet:_errors];
    [_lock unlock];
    return componentErrors;
}

-(NSOrderedSet*)getErrors
{
    [_lock lock];
    NSMutableOrderedSet * set = [[NSMutableOrderedSet alloc]initWithCapacity:_errors.count+1];
    for (GCComponentError * component in _errors) {
        NSError * err = component.error;
        if (err) {
            [set addObject:component.error];
        }
    }
    [_lock unlock];
    
    return set;
}


 -(NSError*)error
{

    
    /*
     if (_errors.count > 0) {
         err = [NSError errorWithDomain:kErrGCCompositeErrorDomain code:kErrGCCompositeErrorCode userInfo:nil];
    }
     */
    
    /*else if(_errors.count > 0) {
        GCComponentError * component = [_errors objectAtIndex:0];
        err = component.error;
    }else{
        err = nil;
        NSAssert(err,@"A Composite error must have a internal error.");
    }*/
    
    [_lock lock];
    NSError * err;
    err = _error;
    NSAssert(err,@"A Composite error must have a internal error.");
    [_lock unlock];
    return err;
}

-(void)addError:(NSError*)error
{
    NSAssert(error,@"Error cannot be nil");
    [_lock lock];
    [_errors addObject:[GCError errorWithError:error sender:nil]];
    [_lock unlock];
}

-(void)addComponentError:(GCComponentError*)error
{
    NSAssert(error,@"Error cannot be nil");
    [_lock lock];
    [_errors addObject:error];
    [_lock unlock];
}

-(NSString *)description
{
    NSMutableString * desc = [NSMutableString stringWithFormat:@"%@ ",[super description]];
    [desc appendFormat:@"childsCount: %d",_errors.count];
    return desc;
}

@end
