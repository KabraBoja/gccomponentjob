//
//  GCJob.h
//  mrbricolage
//
//  Created by Eloi Guzmán on 27/09/13.
//  Copyright (c) 2013 Luis Lopez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GCComponentJob.h"


@interface GCJob : GCComponentJob

#pragma mark - Error management
-(void)addError:(NSError*)error;
-(void)addComponentError:(GCComponentError*)error;

@end