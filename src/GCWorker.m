//
//  GCWorker.m
//  mrbricolage
//
//  Created by Eloi Guzmán on 11/10/13.
//  Copyright (c) 2013 Luis Lopez. All rights reserved.
//

#import "GCWorker.h"
#import "GCJob.h"

@implementation GCWorker

- (id)init
{
    self = [super init];
    if (self) {
        self.status = GCWorkerStatusFree;
        _collectionActiveJobs = [[GCSenderCollection alloc]init];
        //_collectionOfStateDelegatesByJob = [[SenderCollection alloc]initWithWeakReferences];
        //_collectionOfCompletionDelegatesByJob = [[SenderCollection alloc]initWithWeakReferences];
        _queue = [[NSOperationQueue alloc]init];
        _queue.maxConcurrentOperationCount = 1;
    }
    return self;
}

-(void)addJob:(GCComponentJob *)job
{
    NSAssert(job,@"Job cannot be nil");
    //Save the job to avoid being deallocated until the execution finnish
    [_collectionActiveJobs addSender:job forKeyObject:job];

    //Add the delegates
    //TODO:
//    [job.delegatesCompletion addDelegate:self];
//    [job.delegatesState addDelegate:self];

    //Add the operation to the queue
    self.status = GCWorkerStatusWorking;
    [_queue addOperation:job];
}

-(void)dealloc
{
    //Due to async tasks a worker cannot be deallocated until all the remaining jobs are finnished
    NSAssert(_collectionActiveJobs.allSenders.count == 0,@"Deallocating a worker when it still have jobs to do is not allowed");
//    T21LogDealloc(@"");
}

-(void)gc_JobExecuting:(GCComponentJob *)job
{
    if([self.delegateState respondsToSelector:@selector(gc_Worker:jobExecuting:)]){
        [self.delegateState gc_Worker:self jobExecuting:job];
    }
}

-(void)gc_JobFinished:(GCComponentJob *)job
{
    //We remove all the saved delegates and the finnished job.
    [_collectionActiveJobs getSenderWithKeyObject:job deleteEntry:YES];
    
    //Check if the worker is free
    if (_collectionActiveJobs.count == 0) {
        self.status = GCWorkerStatusFree;
    }

    if([self.delegateState respondsToSelector:@selector(gc_Worker:jobFinished:)]){
        [self.delegateState gc_Worker:self jobFinished:job];
    }
}

-(void)gc_JobReady:(GCComponentJob *)job
{
    if([self.delegateState respondsToSelector:@selector(gc_Worker:jobReady:)]){
        [self.delegateState gc_Worker:self jobReady:job];
    }
}

-(void)gc_Job:(GCComponentJob *)job updatedCompletionPercentage:(CGFloat)percent withInfo:(id)completionInfo
{
    
}

@end
