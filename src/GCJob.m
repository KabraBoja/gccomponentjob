//
//  GCJob.m
//  mrbricolage
//
//  Created by Eloi Guzmán on 27/09/13.
//  Copyright (c) 2013 Luis Lopez. All rights reserved.
//

#import "GCJob.h"

@implementation GCJob

-(void)addError:(NSError*)error
{
    [_lock lock];
    if (!_errorComposite) {
        _errorComposite = [GCCompositeError errorWithError:error sender:self];
    }else{
        [_errorComposite addError:[GCError errorWithError:error sender:self]];
    }
    [_lock unlock];
}

-(void)addComponentError:(GCComponentError*)error;
{
    [_lock lock];
    if (!_errorComposite) {
        _errorComposite = [[GCCompositeError alloc]initWithComponentError:error];
    }else{
        [_errorComposite addComponentError:error];
    }
    [_lock unlock];
}


@end
