//
//  AppDelegate.m
//  GCJobs
//
//  Created by Eloi Guzmán on 22/10/13.
//  Copyright (c) 2013 Eloi Guzmán. All rights reserved.
//

#import "AppDelegate.h"
#import "GCCompositeJob.h"
#import "TestJobTimer.h"
#import "GCCompositeError.h"
#import "GCError.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.rootViewController = [[UINavigationController alloc] init];

    self.window.backgroundColor = [UIColor whiteColor];
    
    [self.window makeKeyAndVisible];
        // Override point for customization after application launch.
    [self test];
    return YES;
}

-(void)dealloc
{
    
}

-(void)test
{
    _queue = [[NSOperationQueue alloc]init];
    GCCompositeJob * c1;
    GCCompositeJob * c2;
    GCCompositeJob * c3;
    GCCompositeJob * c4;
    GCJob * j;
    
    /*
     c = [[GCCompositeJob alloc]initWithParent:nil];
    c.delegateState = self;
    
    j = [[TestJobTimer alloc] initWithParent:c];
    [c addJob:j];
    
    j = [[TestJobTimer alloc] initWithParent:c];
    [c addJob:j];
    
    j = [[TestJobTimer alloc] initWithParent:c];
    [c addJob:j];
    
    [_queue addOperation:c];
     */
    
    
    /**
     
     c1 -> (c2,c4)
     c2 -> (j,j,j,c3)
     c3 -> (j,j)
     c4 -> (j,j)
     
     
     */
    
    c1 = [GCCompositeJob jobWithParent:nil identifier:@"c1"];
    c1.delegateState = self;
    c2 = [GCCompositeJob jobWithParent:c1 identifier:@"c2"];
    c3 = [GCCompositeJob jobWithParent:c2 identifier:@"c3"];
    c4 = [GCCompositeJob jobWithParent:c1 identifier:@"c4"];

    
    //c1(c2,c4)
    [c1 addJob:c2];
    [c1 addJob:c4];

    
    //c2(j,j,j,c3)
    j = [TestJobTimer jobWithParent:c2];
    [c2 addJob:j];
    j = [TestJobTimer jobWithParent:c2];
    [c2 addJob:j];
    j = [TestJobTimer jobWithParent:c2];
    [c2 addJob:j];
    [c2 addJob:c3];
    
    //c3(j,j)
    j = [TestJobTimer jobWithParent:c3];
    [c3 addJob:j];
    j = [TestJobTimer jobWithParent:c3];
    [c3 addJob:j];
    
    //c4(j,j)
    j = [TestJobTimer jobWithParent:c4];
    [c4 addJob:j];
    j = [TestJobTimer jobWithParent:c4];
    [c4 addJob:j];
    


    NSError * exampleError = [NSError errorWithDomain:@"domainError" code:0 userInfo:nil];
    GCComponentError * err = [GCError errorWithError:exampleError sender:self];
    GCComponentError * errCopy = [err copy];

    GCCompositeError * composite = [GCCompositeError errorWithComponentError:errCopy sender:c4];
    [composite addError:exampleError];
    [composite addComponentError:[NSError errorWithDomain:@"domainError" code:2 userInfo:nil]];
    
    //GCCompositeError * compCopy = [composite copy];
    

    /*for (int i = 0; i<1000; i++) {
        j = [[TestJobTimer alloc] initWithParent:c];
        [c addJob:j];
    }*/
    
    
    //j = [[TestJobTimer alloc] initWithParent:c];
    //[c addJob:j];
    
    NSLog(@"Job c1: %@", c1);
    [_queue addOperation:c1];
}

-(void)gc_JobFinished:(GCComponentJob *)job
{
    
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
