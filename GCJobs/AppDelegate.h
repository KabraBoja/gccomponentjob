//
//  AppDelegate.h
//  GCJobs
//
//  Created by Eloi Guzmán on 22/10/13.
//  Copyright (c) 2013 Eloi Guzmán. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GCComponentJob.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,GCJobStateProtocol>
{
    GCComponentJob * _rootJob;
    NSOperationQueue * _queue;
}
@property (strong, nonatomic) UIWindow *window;

@end
